import request from '/src/utils/request';

export function getHouseOfType(data = {}) {
  return request({
    url: 'all-house-of-type',
    method: 'get',
    params: data,
  });
}

export function searchHouse(data = {}) {
  return request({
    url: 'search-house',
    method: 'post',
    data: { data },
  });
}

export function getHouseById(data = {}) {
  return request({
    url: 'get-house-by-id',
    method: 'get',
    params: data,
  });
}

export function getAllConvenient() {
  return request({
    url: 'allconvenient',
    method: 'get',
  });
}

export function getAllTypeConvenient() {
  return request({
    url: 'all-type-convenients',
    method: 'get',
  });
}

export function getAllHouseType() {
  return request({
    url: 'alltype',
    method: 'get',
  });
}

export function getAllProvince() {
  return request({
    url: 'all-province',
    method: 'get',
  });
}

export function getAllDistrictOfProvince(data = {}) {
  return request({
    url: 'all-district-of-province',
    method: 'get',
    params: data,
  });
}

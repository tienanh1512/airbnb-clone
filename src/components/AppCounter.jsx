import { CiCircleMinus, CiCirclePlus } from 'react-icons/ci';

function AppCounter({
  value,
  onIncrease,
  onDecrease,
  disableDecrease,
  disableIncrease,
}) {
  return (
    <div className="flex items-center justify-between w-[104px]">
      <button disabled={disableDecrease} role="button" onClick={onDecrease}>
        <CiCircleMinus
          className={`${
            value && !disableDecrease ? 'text-gray-500' : 'text-gray-200'
          }`}
          size={32}
        />
      </button>
      <div>{value} </div>
      <button disabled={disableIncrease} onClick={onIncrease}>
        <CiCirclePlus size={32} className="text-gray-500" />
      </button>
    </div>
  );
}

export default AppCounter;

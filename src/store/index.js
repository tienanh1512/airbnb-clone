import { configureStore } from '@reduxjs/toolkit';
import searchSlice from './reducers/searchSlice';
import authSlice from './reducers/authSlice';
import appSlice from './reducers/appSlice';

const store = configureStore({
  reducer: {
    search: searchSlice.reducer,
    auth: authSlice.reducer,
    app: appSlice.reducer,
  },
});

export default store;

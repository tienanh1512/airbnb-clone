import { useEffect, useState, useCallback } from 'react';
import Modal from '@/components/modal/Modal';
import { getAllTypeConvenient } from '@/api/houseApi';

function AllConvenientModal({ allConvenient, ...props }) {
  const [allType, setAllType] = useState([]);

  useEffect(() => {
    fnGetAllType();
  }, []);

  const fnGetAllType = useCallback(async () => {
    const res = await getAllTypeConvenient();

    setAllType(res?.data?.data);
  });

  return (
    <Modal {...props} title="Nơi này có những gì cho bạn">
      <div>
        {allType?.map((type, typeIndex) => {
          return (
            <div key={`type_${typeIndex}`} className="mb-10">
              <h3 className="text-xl font-semibold">{type}</h3>
              <ul className="">
                {allConvenient?.map((convenient, index) => {
                  if (convenient?.typeConvenient != type) return;
                  return (
                    <li
                      key={`convenient_${index}`}
                      className="border-b-[1px] py-6"
                    >
                      {convenient?.name}
                    </li>
                  );
                })}
              </ul>
            </div>
          );
        })}
      </div>
    </Modal>
  );
}

export default AllConvenientModal;

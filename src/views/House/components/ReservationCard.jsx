import { useState, useEffect, useRef, useMemo } from 'react';
import { addDays } from 'date-fns';
import { AiFillStar } from 'react-icons/ai';
import { IoIosArrowDown } from 'react-icons/io';
import { toast } from 'react-toastify';
import DatePicker from '@/components/DatePicker';
import Button from '@/components/Button';
import SearchOptionWrapper from '@/components/navbar/SearchOptionWrapper';
import GuestSearchWrapper from '@/components/navbar/GuestSearchWrapper';
import AppCounter from '@/components/AppCounter';
import useOutsideClick from '@/hooks/useOutsideClick';
import {
  calculateDateDifference,
  dateStrFormatter_YYYYMMDD,
} from '@/utils/dateFormatUtils';
import { createContact } from '@/api/customerApi';

function ReservationCard({ houseData }) {
  console.log('House data', houseData);
  const [reservationData, setReservationData] = useState({
    houseId: houseData?.id,
    arriveDate: new Date(),
    leftDate: addDays(new Date(), 1),
    price: houseData?.price * 2,
    numberOver13: 1,
    numberUnder13: 0,
    numberChildren: 0,
    numberAnimal: 0,
    haveAnimals: false,
  });

  useEffect(() => {
    setReservationData({
      ...reservationData,
      houseId: houseData?.id,
      price: houseData?.price * 2,
    });
  }, [houseData]);

  useEffect(() => {
    let price =
      (calculateDateDifference(
        reservationData.leftDate,
        reservationData.arriveDate,
      ) +
        1) *
      houseData?.price;
    setReservationData({
      ...reservationData,
      price: price,
    });
  }, [reservationData.arriveDate, reservationData.leftDate]);

  const handleSetValue = (name, val) => {
    setReservationData({
      ...reservationData,
      [name]: val,
    });
  };

  const handleChangeDate = select => {
    setReservationData({
      ...reservationData,
      arriveDate: select.startDate,
      leftDate: select.endDate,
    });
  };

  const [showGuestInputs, setShowGuestInputs] = useState(false);

  const guestInputsRef = useOutsideClick(() => setShowGuestInputs(false));

  const handleCreateContract = async () => {
    console.log('Submit: ', reservationData);
    const formData = {
      ...reservationData,
      arriveDate: dateStrFormatter_YYYYMMDD(reservationData.arriveDate),
      leftDate: dateStrFormatter_YYYYMMDD(reservationData.leftDate),
      haveAnimals: reservationData.numberAnimal > 0,
    };
    const res = await createContact(formData);
    if (!res?.data?.errCode) {
      toast.success('Tạo hợp đồng thành công');
    } else {
      toast.error(res?.data?.errMessag);
    }
  };

  return (
    <div className="sticky top-20">
      <div className="card w-full bg-base-100 shadow-xl card-bordered border-neutral-200 mt-12">
        <div className="card-body">
          <div className="flex flex-col">
            <div className="flex justify-between">
              <div>
                <span className="text-xl font-semibold">
                  ${houseData?.price}
                </span>{' '}
                / đêm{' '}
              </div>
              <div>
                <div className="flex mr-2 items-center">
                  <AiFillStar />
                  <span className="pl-[2px] font-medium">
                    {houseData?.star}
                    <span className="px-1">&middot;</span>
                  </span>
                  <span className="underline font-medium text-neutral-400">
                    {houseData?.countReview} đánh giá
                  </span>
                </div>
              </div>
            </div>
            <div className="mt-2">
              <div className="border rounded-t-lg overflow-hidden px-2">
                <DatePicker
                  startDate={reservationData.arriveDate}
                  endDate={reservationData.leftDate}
                  monthColumn={1}
                  // showDateDisplay={true}
                  onChange={handleChangeDate}
                />
              </div>

              {/* Guest Input Popover */}
              <div
                className="px-3 pb-3 pt-2 rounded-b-lg border cursor-pointer flex items-center relative"
                onClick={() => setShowGuestInputs(true)}
              >
                <div className="flex flex-col flex-1 ">
                  <span className="font-medium text-xs">Khách</span>
                  {/* <span className="text-sm">2 khách, 2 em bé, 2 thú cưng</span> */}
                </div>
                <IoIosArrowDown size={20} />

                {/* Guest Input Wrapper */}
                <SearchOptionWrapper
                  ref={guestInputsRef}
                  className={`z-10 bottom-[100%] w-full left-0 border ${
                    showGuestInputs ? 'block' : 'hidden'
                  }`}
                >
                  <GuestSearchWrapper
                    label="Người lớn"
                    description="Từ 13 tuổi trở lên"
                    borderBottom
                  >
                    <AppCounter
                      onIncrease={() => {
                        handleSetValue(
                          'numberOver13',
                          reservationData.numberOver13 + 1,
                        );
                      }}
                      onDecrease={() => {
                        handleSetValue(
                          'numberOver13',
                          reservationData.numberOver13 - 1,
                        );
                      }}
                      value={reservationData.numberOver13}
                    ></AppCounter>
                  </GuestSearchWrapper>
                  <GuestSearchWrapper
                    label="Trẻ em"
                    description="Độ tuổi 2 - 12"
                    borderBottom
                  >
                    <AppCounter
                      onIncrease={() => {
                        handleSetValue(
                          'numberUnder13',
                          reservationData.numberUnder13 + 1,
                        );
                      }}
                      onDecrease={() => {
                        handleSetValue(
                          'numberUnder13',
                          reservationData.numberUnder13 - 1,
                        );
                      }}
                      value={reservationData.numberUnder13}
                    ></AppCounter>
                  </GuestSearchWrapper>
                  <GuestSearchWrapper
                    label="Em bé"
                    description="Dưới 2 tuổi"
                    borderBottom
                  >
                    <AppCounter
                      onIncrease={() => {
                        handleSetValue(
                          'numberChildren',
                          reservationData.numberChildren + 1,
                        );
                      }}
                      onDecrease={() => {
                        handleSetValue(
                          'numberChildren',
                          reservationData.numberChildren - 1,
                        );
                      }}
                      value={reservationData.numberChildren}
                    ></AppCounter>
                  </GuestSearchWrapper>
                  <GuestSearchWrapper label="Thú cưng" description="">
                    <AppCounter
                      onIncrease={() => {
                        handleSetValue(
                          'numberAnimal',
                          reservationData.numberAnimal + 1,
                        );
                      }}
                      onDecrease={() => {
                        handleSetValue(
                          'numberAnimal',
                          reservationData.numberAnimal - 1,
                        );
                      }}
                      value={reservationData.numberAnimal}
                    ></AppCounter>
                  </GuestSearchWrapper>
                </SearchOptionWrapper>
              </div>
            </div>
          </div>
          <div className="card-actions justify-end mt-2">
            <Button label="Đặt phòng" onClick={handleCreateContract}></Button>
          </div>
          <div className="border-t-[1px] mt-4">
            <div className="flex justify-between items-center pt-4">
              <h3 className="text-lg font-semibold">Tổng trước thuế</h3>
              <span>${reservationData.price}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ReservationCard;
